@extends('layouts.app')

@section('content')

    <section class="register_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-11 py-5">
                    <h3 class="text-center bg-dark text-white py-4 font-weight-bold ">Congratulations!</h3>

                    <p class="pt-3 text-center">You have successfully registered. A verification email has been sent to you. Please follow the instructions in order to activate your account.</p>
                </div>
            </div>
        </div>
    </section>

{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registration Confirmation') }}</div>

                <h3>Congratulations Your Account are Create!</h3>

                <p>You have successfully registered. A verification email has been sent to you. Please follow the instructions in order to activate your account.</p>

                --}}{{--<div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>--}}{{--
            </div>
        </div>
    </div>
</div>--}}
@endsection
