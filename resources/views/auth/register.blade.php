@extends('layouts.app')

@section('content')
    <section class="register_section">
        <div class="flash-message">
            @foreach(['danger','warning','success','info'] as $msg)
                @if(Session::has('alert'. $msg))
                    <p class="alert alert{{ $msg }}">{{ Session::get('alert'. $msg) }}
                        <a href="#" class="close" data-dimiss="alert" aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-11">
                    <div class="register_form_inner">
                        <h2>Register</h2>
                    <div class="register_form">
                        <div class="form_item">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <h3 class="font-weight-bold">Account Information</h3>
                                <div class="row">
                                    <div class="col-md-6 single-input">
                                        <label for="email">Email *</label>
                                        <input type="email" name="email" required="required" placeholder="{{__('Email')}}">
                                        @if ($errors->has('email'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input">
                                        <label for="phone">Telephone Number</label>
                                        <input type="text" name="phone" placeholder="{{__('Telephone Number')}}">
                                        @if ($errors->has('phone'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('phone') }}</strong> </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input">
                                        <label for="password">Password *</label>
                                        <input type="password" name="password" required="required" placeholder="{{__('Password')}}">
                                        @if ($errors->has('password'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input">
                                        <label for="password_confirmation">Confirm Password *</label>
                                        <input type="password" name="password_confirmation" placeholder="{{__('Confirm Password')}}">
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('password_confirmation') }}</strong> </span>
                                        @endif
                                    </div>
                                </div>
                                <h3 class="font-weight-bold">Personal Information</h3>
                                <div class="row">
                                    <div class="col-md-6 single-input">
                                        <label for="first_name">First Name *</label>
                                        <input type="text" name="first_name" required="required" placeholder="{{__('First Name')}}">
                                        @if ($errors->has('first_name'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input">
                                        <label for="last_name">Last Name *</label>
                                        <input type="text" name="last_name" required="required" placeholder="{{__('Last Name')}}">
                                        @if ($errors->has('last_name'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input">
                                        <label for="gender">Gender *</label>
                                        <select name="gender" required="required">
                                            <option value="">Choose Gender</option>
                                            @foreach($genders as $gender)
                                                <option value="{{ $gender->id }}" {{ old('gender') == $gender->id ? 'selected' : '' }}>{{ $gender->gender }}</option>
                                            @endforeach

                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('gender') }}</strong> </span>
                                        @endif
                                    </div>

                                    <div class="col-md-6 single-input">
                                        <label for="country">Country *</label>
                                        <select name="country" required="required">
                                            <option value="">Choose a Country</option>
                                            <option value="">Bangladesh</option>
                                            <option value="">US</option>
                                            <option value="">Uk</option>
                                            <option value="">Canada</option>

                                        </select>
                                        @if ($errors->has('gender'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('gender') }}</strong> </span>
                                        @endif
                                    </div>

                                    <div class="col-md-6 single-input">
                                        <label for="province">Province *</label>
                                        <select name="province" required="required">
                                            <option value="">Choose A Province</option>
                                            <option value="">Province One</option>
                                            <option value="">Province Tow</option>
                                            <option value="">Province Three</option>

                                        </select>
                                        @if ($errors->has('province'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('province') }}</strong> </span>
                                        @endif
                                    </div>

                                    <div class="col-md-6 single-input{{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label for="city">City *</label>
                                        <input type="text" name="city" required="required" placeholder="{{__('City')}}">
                                        @if ($errors->has('city'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('city'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('city') }}</strong> </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6 single-input{{ $errors->has('street_address') ? ' has-error' : '' }}">
                                        <label for="city">Street Address *</label>
                                        <input type="text" name="street_address" required="required" placeholder="{{__('Street Address')}}">
                                        @if ($errors->has('street_address'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('street_address') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('street_address'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('street_address') }}</strong> </span>
                                        @endif
                                    </div>

                                    <div class="col-md-6 single-input{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                        <label for="postal_code">Postal Code</label>
                                        <input type="text" name="postal_code" placeholder="{{__('Postal Code')}}">
                                        @if ($errors->has('postal_code'))
                                            <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('postal_code') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('postal_code'))
                                            <span class="help-block custom-help-block"> <strong>{{ $errors->first('postal_code') }}</strong> </span>
                                        @endif
                                    </div>
                                    <div class="single-submit-button">
                                        <input type="submit" value="Register">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<{{--div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
