@extends('layouts.app')

@section('content')

    <section class="register_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-11">
                    <div class="register_form_inner">
                        <h2>Reset Password</h2>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                        <div class="register_form">
                            <div class="form_item">
                                <form method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 single-input">
                                            <label for="email">E-Mail</label>
                                            <input type="email" name="email" required="required" placeholder="{{__('E-Mail')}}">
                                            @if ($errors->has('email'))
                                                <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        {{--<div class="col-md-12 single-input">
                                            <label for="password">Password *</label>
                                            <input type="password" name="password" required="required" placeholder="{{__('Password')}}">
                                            @if ($errors->has('password'))
                                                <span class="help-block custom-help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>--}}
                                    </div>

                                    <div class="single-submit-button">
                                        <input type="submit" value="Send Password Reset Link">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-11 mt-5">
                        <div class="text-center"><i class="fa fa-user" aria-hidden="true"></i> {{__('Have an Account?')}}? <a href="{{route('login')}}">{{__('Sign in')}}</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
