@extends('layouts.app')

@section('content')
    <section class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-12">
                    @include('user.user_dashboard_menu')

                </div>


                <div class="col-md-9 col-12">
                    <div class="dashboard_content">
                        <div class="user_profile">
                          {{--  <div class="user_profile_edit_btn">
                                <a href="#">Edit Profile</a>
                            </div>--}}
                            <div class="user_profile_top">
                                <div class="row">
                                    <div class="col-md-2 col-12">
                                        <div class="user_profile_image">
                                            <img src="{{asset('/')}}images/man.png" alt="User Image">
                                        </div>

                                    </div>
                                    <div class="col-md-10 col-12">
                                        <div class="user_details">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <h2 class="user_name">{{ $user->name }}</h2>
                                                    @if($user->street_address && $user->postal_code)
                                                        <p> <i class="fa fa-map-marker"></i></i> {{ $user->street_address }}, {{ $user->postal_code }}</p>
                                                    @endif
                                                    <p> <i class="fa fa-phone"></i> {{ $user->phone }}</p>
                                                    <p><i class="fa fa-envelope"></i> {{ $user->email }}</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="float-right pr-3" style="padding-top: 70px;">
                                                        <p>Created {{ $user->created_at->format('j F, Y') }}</p>
                                                        <p>Updated {{ $user->updated_at->format('j F, Y') }} </p>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-12">
                           <div class="order_list">
                               <i class="fa fa-shopping-cart"></i>
                               <h3>Order</h3>
                           </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="wish_list">
                                <i class="fa fa-heart"></i>
                                <h3>WishList</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
