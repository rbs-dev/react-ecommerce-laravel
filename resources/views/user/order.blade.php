@extends('layouts.app')

@section('content')
    <section class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-12">
                    @include('user.user_dashboard_menu')

                </div>


                <div class="col-md-9 col-12">
                    <div class="dashboard_content my-5 ">
                        <div class="register_form_inner">
                            <h2>Order List</h2>
                            <div class="register_form">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="table_header_bg">Created</th>
                                        <th class="table_header_bg">Delivery</th>
                                        <th class="table_header_bg">Invoice</th>
                                        <th class="table_header_bg">Item</th>
                                        <th class="table_header_bg">Shipping</th>
                                        <th class="table_header_bg">Process</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>15-07-2021</td>
                                            <td>15-07-2021</td>
                                            <td>42107005</td>
                                            <td>2</td>
                                            <td>60</td>
                                            <td>Processing</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>


    {{--<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
