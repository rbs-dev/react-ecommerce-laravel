@extends('layouts.app')

@section('content')
    <section class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-12">
                    @include('user.user_dashboard_menu')

                </div>


                <div class="col-md-9 col-12">
                    <div class="dashboard_content my-5 ">
                        <div class="register_form_inner">
                            <h2>Edit Profile</h2>
                            <div class="register_form">
                                <div class="form_item">
                                    <form method="POST" action="{{ route('update.profile') }}">
                                        @csrf
                                        <h3 class="font-weight-bold">Account Information</h3>
                                        <div class="row">
                                            <div class="col-md-6 single-input">
                                                <label for="email">Email *</label>
                                                <input type="email" name="email" disabled required="required" value="{{ $user->email }}" placeholder="{{__('Email')}}">
                                                @if ($errors->has('email'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6 single-input">
                                                <label for="phone">Telephone Number</label>
                                                <input type="text" name="phone" value="{{ $user->phone }}" placeholder="{{__('Telephone Number')}}">
                                                @if ($errors->has('phone'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('phone') }}</strong> </span>
                                                @endif
                                            </div>
                                        </div>
                                        <h3 class="font-weight-bold">Personal Information</h3>
                                        <div class="row">
                                            <div class="col-md-6 single-input">
                                                <label for="first_name">First Name *</label>
                                                <input type="text" name="first_name" required="required" value="{{ $user->first_name }}" placeholder="{{__('First Name')}}">
                                                @if ($errors->has('first_name'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6 single-input">
                                                <label for="last_name">Last Name *</label>
                                                <input type="text" name="last_name" required="required" value="{{ $user->last_name }}" placeholder="{{__('Last Name')}}">
                                                @if ($errors->has('last_name'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6 single-input">
                                                <label for="gender">Gender *</label>
                                                <select name="gender" required="required">
                                                    <option value="">Choose Gender</option>
                                                    @foreach($genders as $gender)
                                                        <option value="{{ $gender->id }}" {{ $user->gender && $user->gender->id == $gender->id ? 'selected' : '' }}>{{ $gender->gender }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('gender'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('gender') }}</strong> </span>
                                                @endif
                                            </div>

                                            <div class="col-md-6 single-input">
                                                <label for="country">Country *</label>
                                                <select name="country" required="required">
                                                    <option value="">Choose a Country</option>
                                                    <option value="">Bangladesh</option>
                                                    <option value="">US</option>
                                                    <option value="">Uk</option>
                                                    <option value="">Canada</option>

                                                </select>
                                                @if ($errors->has('gender'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('gender') }}</strong> </span>
                                                @endif
                                            </div>

                                            <div class="col-md-6 single-input">
                                                <label for="province">Province *</label>
                                                <select name="province" required="required">
                                                    <option value="">Choose A Province</option>
                                                    <option value="">Province One</option>
                                                    <option value="">Province Tow</option>
                                                    <option value="">Province Three</option>

                                                </select>
                                                @if ($errors->has('province'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('province') }}</strong> </span>
                                                @endif
                                            </div>

                                            <div class="col-md-6 single-input{{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="city">City *</label>
                                                <input type="text" name="city" required="required" {{ $user->city }} placeholder="{{__('City')}}">
                                                @if ($errors->has('city'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                                @endif
                                                @if ($errors->has('city'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('city') }}</strong> </span>
                                                @endif
                                            </div>
                                            <div class="col-md-6 single-input{{ $errors->has('street_address') ? ' has-error' : '' }}">
                                                <label for="city">Street Address *</label>
                                                <input type="text" name="street_address" value="{{ $user->street_address }}" required="required" placeholder="{{__('Street Address')}}">
                                                @if ($errors->has('street_address'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('street_address') }}</strong>
                                    </span>
                                                @endif
                                                @if ($errors->has('street_address'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('street_address') }}</strong> </span>
                                                @endif
                                            </div>

                                            <div class="col-md-6 single-input{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                                                <label for="postal_code">Postal Code</label>
                                                <input type="text" name="postal_code" value="{{ $user->postal_code }}" placeholder="{{__('Postal Code')}}">
                                                @if ($errors->has('postal_code'))
                                                    <span class="help-block custom-help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                                @endif
                                                @if ($errors->has('postal_code'))
                                                    <span class="help-block custom-help-block"> <strong>{{ $errors->first('postal_code') }}</strong> </span>
                                                @endif
                                            </div>
                                            <div class="single-submit-button">
                                                <input type="submit" value="Update Information">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>


    {{--<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
