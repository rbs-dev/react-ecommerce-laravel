<div class="dashboard_site_bar">
    <ul>
        <li class="{{ Request::is('home') ? 'active' : ''}}">
            <a href="{{route('home')}}">
                <div class="d-icon">
                    <img src="{{asset('/')}}images/dash.png" alt="">
                </div>
                Dashboard
            </a>
        </li>

        <li class="{{ Request::is('show-profile') ? 'active' : ''}}">
            <a href="{{route('show.profile')}}">
                <div class="d-icon">
                    <img src="{{asset('/')}}images/ps.png" alt="">
                </div>
                Edit Profile
            </a>
        </li>

        <li class="{{ Request::is('order') ? 'active' : ''}}">
            <a href="{{route('order')}}">
                <div class="d-icon">
                    <img src="{{asset('/')}}images/mr.png" alt="">
                </div>
                Order
            </a>
        </li>

        {{--<li class="menu-item-has-children tab-dropdown">
            <div class="d-icon"><img src="{{asset('/')}}images/cog.png" alt=""><strong style="font-weight: bold">Setting</strong></div>
            <ul class="sub-menu" style="{{ Request::is('account/delete/reason') ? 'display: block;' : ''}}{{ Request::is('change/password') ? 'display: block;' : ''}}{{ Request::is('email/preference') ? 'display: block;' : ''}}">
                <li class="{{ Request::is('account/delete/reason') ? 'active' : ''}}">
                    <a style="pointer-events: {{ auth()->user()->is_suspended == 1 ? 'none' : '' }}" href="#">
                        Delete My Account
                    </a>
                </li>

                <li class="{{ Request::is('change/password') ? 'active' : ''}}">
                    <a style="pointer-events: {{ auth()->user()->is_suspended == 1 ? 'none' : '' }}" href="#">
                        Change Password
                    </a>
                </li>

                <li class="{{ Request::is('email/preference') ? 'active' : ''}}">
                    <a style="pointer-events: {{ auth()->user()->is_suspended == 1 ? 'none' : '' }}" href="#">
                        Email Preferences
                    </a>
                </li>
            </ul>
        </li>--}}

        <li>
            <a href="#">
                <div class="d-icon">
                    <img src="{{asset('/')}}images/cog.png" alt="">
                </div>
                Settings
            </a>
        </li>

        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <div class="d-icon">
                    <img src="{{asset('/')}}images/cog.png" alt="">
                </div>
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    </ul>
</div>
