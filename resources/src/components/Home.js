import React from 'react';
import Header from "./home/Header";
import Content from "./Content";
import Products from "./Products";

function Home() {
    return (
        <div>
            <Header />
            <Content />
            <Products />
        </div>
    );
}

export default Home;

