import React from 'react';
import ReactDOM from 'react-dom';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import {Row,Col} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFish, faAppleAlt, faMugHot,faCat, faHome,faTshirt,faChair,faBook,faPills} from '@fortawesome/free-solid-svg-icons'

import img01 from '../images/slider_one.jpg'
import img02 from '../images/slider_two.jpg'

function Example() {
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    return (
        <div className="container">
            <br/>
            <div className="row">
                <div className="col-11 col-md-4">
                   <div className="row">
                       <div className="col-md-6">
                           <div className="category_item text-center">
                               <div className="mx-auto py-3">
                                   <FontAwesomeIcon icon={faAppleAlt} />
                                   <p>Fruits & Vegetables</p>
                               </div>
                           </div>
                       </div>
                       <div className="col-md-6">
                           <div className="category_item text-center">
                               <div className="mx-auto py-3">
                                   <FontAwesomeIcon icon={faFish} />
                                   <p>Meat & Fish</p>
                               </div>
                           </div>
                       </div>
                   </div>
                    <br/>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="category_item text-center">
                                <div className="mx-auto py-3">
                                    <FontAwesomeIcon icon={faMugHot} />
                                    <p>Snacks</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="category_item text-center">
                                <div className="mx-auto py-3">
                                    <FontAwesomeIcon icon={faCat} />
                                    <p>Pet Care</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="category_item text-center">
                                <div className="mx-auto py-3">
                                    <FontAwesomeIcon icon={faHome} />
                                    <p>Home & Cleaning</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="category_item text-center">
                                <div className="mx-auto py-3">
                                    <FontAwesomeIcon icon={faAppleAlt} />
                                    <p>Dairy</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="col-11 col-md-8">
                    <Slider {...settings}>
                        <div>
                            <img src={img01} width={730} alt="Image One"/>
                        </div>
                        <div>
                            <img src={img02} width={730} alt="Image Two"/>
                        </div>
                    </Slider>
                </div>
            </div>
        </div>
    );
}

export default Example;
