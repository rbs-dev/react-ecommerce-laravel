import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestionCircle, faAppleAlt, faLanguage,faBirthdayCake, faBriefcase,faTshirt,faChair,faBook,faPills} from '@fortawesome/free-solid-svg-icons'
import {Navbar,Container,Nav,NavDropdown,Form,FormControl,Button} from 'react-bootstrap';

import {
    Link
} from "react-router-dom";

class Header extends Component {
    render() {
        return (
            <div>
                <Navbar bg="white" expand="lg">
                    <Container>
                        <Navbar.Brand href="#">E-Commerce</Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll" />
                        <Navbar.Collapse id="navbarScroll">
                            <Nav
                                className="mr-auto my-2 my-lg-0 nav_bar_color"
                                style={{ maxHeight: '100px' }}
                                navbarScroll
                            >
                                <NavDropdown className="nav_drop_down" title={
                                    <span><FontAwesomeIcon icon={faAppleAlt} /> Grocery</span>
                                } id="navbarScrollingDropdown">
                                    <NavDropdown.Item className="dropdown-menu-item" href="#action/3.1"><FontAwesomeIcon icon={faBirthdayCake} /> Bakery</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">
                                        <FontAwesomeIcon icon={faBriefcase} /> Bags
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        <FontAwesomeIcon icon={faTshirt} /> Clothing
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        <FontAwesomeIcon icon={faChair} /> Furniture
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        <FontAwesomeIcon icon={faBook} /> Book
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">
                                        <FontAwesomeIcon icon={faPills} /> Medicine
                                    </NavDropdown.Item>
                                </NavDropdown>

                                <Form className="search_box">

                                    <FormControl
                                        type="search"
                                        placeholder="Search your products from here"
                                        className="mr-2"
                                        aria-label="Search"
                                    />
                                </Form>
                            </Nav>
                            <Nav className="d-flex nav_bar_color">
                                <Nav.Link href="#action2">Offer</Nav.Link>
                                <Nav.Link href="#action2">
                                    <FontAwesomeIcon icon={faQuestionCircle} /> Need help
                                </Nav.Link>

                                <NavDropdown className="nav_drop_down" title={
                                    <span><FontAwesomeIcon icon={faLanguage} /> English</span>
                                } id="navbarScrollingDropdown">
                                    <NavDropdown.Item href="#action3">Bangla</NavDropdown.Item>
                                    <NavDropdown.Item href="#action4">English</NavDropdown.Item>
                                </NavDropdown>
                                <a href="#">
                                    <button type="button" className="login_btn"> Login</button>
                                </a>

                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </div>
        );
    }
}

export default Header;
