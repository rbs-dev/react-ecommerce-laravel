import React,{useState,useEffect} from 'react';
import {Container,Row,Col,Card,Button} from 'react-bootstrap';

function Products()
{
    const [data,setData] = useState([]);

    const [counter, setCounter] = useState(1);

    const incrementCounter = (id) => setCounter(counter + 1);
    let decrementCounter = (id) => setCounter(counter - 1);
    if(counter<=1) {
        decrementCounter = (id) => setCounter(1);
    }
    const getData = async () => {
        //const response = await fetch("https://api.github.com/users");
        //const response = await fetch("https://fakestoreapi.com/products");
        const response = await fetch("https://res.cloudinary.com/sivadass/raw/upload/v1535817394/json/products.json");
        /*const response = await fetch("http://www.terminalbd.com/android-api-ecommerce/all-brand",{
            method: 'GET',
            headers: {
                "Postman-Token": "<calculated when request is sent>",
                "Host": "<calculated when request is sent>",
                "User-Agent": "PostmanRuntime/7.26.8",
                "Accept": "*!/!*",
                'Access-Control-Allow-Origin': '*',
                'Content-type': 'application/json',
                "Accept-Encoding": "gzip, deflate, br",
                "Connection": "keep-alive",
                "X-API-KEY": "terminalbd",
                "X-API-VALUE": "terminalbd@aps",
                "X-API-SECRET": "UBE49VBB"
            }
        });*/

        setData(await response.json());
    }

    useEffect(()=>{
        getData();
    },[]);


    return(
        <div>
            <Container>
                <br/>
                <Row xs={1} md={4} className="g-4">
                    {
                        data.map((item,id)=>
                            <Col>
                                <Card>
                                    <Card.Img variant="top" style={{height: 200}} src={item.image} />
                                    <Card.Body>
                                        <Card.Text className="text-center p-0 m-0">{item.name}</Card.Text>
                                        <Card.Title className="text-center">
                                            ${item.price}
                                        </Card.Title>
                                        <h5>{item.type}</h5>

                                        <div className="add-minus-quantity">
                                            <i className="fas fa-minus" onClick={ () => decrementCounter(id)}></i>
                                            <input type="text" placeholder={counter}/>
                                            <i className="fas fa-plus" onClick={ () => incrementCounter(id)}></i>
                                        </div>

                                        <Button style={{width: 210}}>ADD TO CART</Button>
                                    </Card.Body>

                                </Card>
                                <br/>
                            </Col>
                        )}
                </Row>
                <br/>
            </Container>
        </div>
    )

}

export default Products;
