/*import React, {useContext} from "react";
import {CartDispatchContext, CartStateContext, removeFromCart, incrementProductQty,decrementProductQty} from '../contexts/cart';

const Order = () => {
    const { items } = useContext(CartStateContext);
    const dispatch = useContext(CartDispatchContext);
    const handleRemove = (productId) => {
        return removeFromCart(dispatch, productId);
    };

   /!* const cartIncrement = (productId) => {
        return incrementProductQty(dispatch, productId);
    };

    const cartDecrement = (productId) => {
        return decrementProductQty(dispatch, productId);
    }*!/

    return(
        <>
            <section className="order-page">
                <div className="orderPage-wrapper">
                    <div className="order-info">
                        <h2>Order List</h2>
                        <ul className="cart-items">
                            {items.map((product) => {
                                return (
                                    <li className="cart-item" key={product.name}>
                                        <img className="product-image" src={product.image} />
                                        <div className="product-info">
                                            <p className="product-name">{product.name}</p>
                                           {/!* <a onClick={() => cartIncrement(product.id)}>+</a>
                                            <a onClick={() => cartDecrement(product.id)}>-</a>*!/}
                                            <p className="product-price">{product.price}</p>
                                        </div>
                                        <div className="product-total">
                                            <p className="quantity">
                                                {`${product.quantity} ${
                                                    product.quantity > 1 ? "Nos." : "No."
                                                }`}
                                            </p>
                                            <p className="amount">{product.quantity * product.price}</p>
                                        </div>
                                       {/!* <button
                                            className="product-remove"
                                            onClick={() => handleRemove(product.id)}
                                        >
                                            ×
                                        </button>*!/}
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                </div>
            </section>

        </>
    );
}

export default Order;*/

import React, {useContext,} from "react";
import classNames from "classnames";
import { CartStateContext } from "../contexts/cart";


const OrderPage = () => {
    const { items = [] } = useContext(CartStateContext);
    const totalItems = items.length;
    const cartTotal = items
        .map((item) => item.price * item.quantity)
        .reduce((prev, current) => prev + current, 0);


    return (
        <div className="checkout-page">
            <div className="container">
                <div className="order-details">
                    <div className="detail-container">
                        <h2>Order</h2>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Name"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Email"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Address"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="State"/>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Phone"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="City"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="ZIP/Postal Code"/>
                                </div>
                                <div className="form-group">
                                <input type="text" className="form-control" placeholder="Country"/>
                            </div>

                            </div>
                        </div>

                        <div className="actions">
                            <button>
                                Save Address
                            </button>
                            <button>
                                Procced
                            </button>

                        </div>
                    </div>
                </div>
                <div className="order-summary">
                    <h2>
                        Summary
                        <span>{` (${totalItems}) Items`}</span>
                    </h2>
                    <ul className="cart-items">
                        {items.map((product) => {
                            return (
                                <li className="cart-item" key={product.name}>
                                    <img className="product-image" src={product.image} />
                                    <div className="product-info">
                                        <p className="product-name">{product.name}</p>
                                        <p className="product-price">{product.price}</p>
                                    </div>
                                    <div className="product-total">
                                        <p className="quantity">
                                            {`${product.quantity} ${
                                                product.quantity > 1 ? "Nos." : "No."
                                            }`}
                                        </p>
                                        <p className="amount">{product.quantity * product.price}</p>
                                    </div>
                                </li>
                            );
                        })}
                    </ul>

                    <ul className="total-breakup">
                        <li>
                            <p>Subtotal</p>
                            <p>{cartTotal}</p>
                        </li>
                        <li>
                            <h2>Total</h2>
                            <h2>{cartTotal}</h2>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default OrderPage;

