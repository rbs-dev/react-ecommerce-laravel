import React, { useEffect, useContext,useState } from "react";
import Search from "../components/Search";

import {
    ProductsStateContext,
    ProductsDispatchContext,
    getProducts
} from "../contexts/products";
import { CommonStateContext } from "../contexts/common";

const SearchPage = () => {
    const { products, isLoading, isLoaded } = useContext(ProductsStateContext);
    const { searchKeyword } = useContext(CommonStateContext);
    const dispatch = useContext(ProductsDispatchContext);

    const productsList =
        products &&
        products.filter((product) => {
            return (
                product.name.toLowerCase().includes(searchKeyword.toLowerCase()) ||
                !searchKeyword
            );
        });

    useEffect(() => {
        getProducts(dispatch);
    }, []);



    if (isLoading) {
        return (
            <div className="products-wrapper">
                <h1>Loading...</h1>
            </div>
        );
    }

    return (
        <>
            <div className="products-wrapper">
                <div className="products">
                    {isLoaded &&
                    productsList.map((data) => {
                        return <Search key={data.id} data={data} />;
                    })}
                </div>
            </div>
        </>
    );
};

export default SearchPage;

