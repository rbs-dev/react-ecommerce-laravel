import React, { useEffect, useContext,useState } from "react";
import ProductCard from "../components/Product";

import {
  ProductsStateContext,
  ProductsDispatchContext,
  getProducts
} from "../contexts/products";
import { CommonStateContext } from "../contexts/common";

import Slider from "../components/Slider";
import FeatureCategory from "../components/FeatureCategory";

const Home = () => {
  const { products, isLoading, isLoaded } = useContext(ProductsStateContext);
  const { searchKeyword } = useContext(CommonStateContext);
  const dispatch = useContext(ProductsDispatchContext);

    const [currentPage,setCurrentPage] = useState(1);
    const [itemPerPage, setItemPerPage] = useState(8);

    const [pageNumberLimit, setPageNumberLimit] = useState(5);
    const [maxPageNumberLimit, setMaxPageNumberLimit] = useState(5);
    const [minPageNumberLimit, setMinPageNumberLimit] = useState(0);

    const handleClick = (e) => {
        setCurrentPage(Number(e.target.id));
    }

    const indexOfLastItem = currentPage * itemPerPage;
    const indexOfFirstItem = indexOfLastItem - itemPerPage;
    const currentItems = products.slice(indexOfFirstItem,indexOfLastItem);

    const pages = [];
    for (let i=1; i<=Math.ceil(products.length/itemPerPage); i++)
    {
        pages.push(i);
    }
    const renderPageNumbers = pages.map(number => {
        if(number < maxPageNumberLimit +1 && number > minPageNumberLimit){
            return(
                <li
                    key={number}
                    id={number}
                    onClick={handleClick}
                    className = { currentPage == number ? "active" : null}
                >
                    {number}
                </li>
            );
        } else {
            return null;
        }
    });

  const productsList =
    products &&
    products.filter((product) => {
      return (
        product.name.toLowerCase().includes(searchKeyword.toLowerCase()) ||
        !searchKeyword
      );
    });

  useEffect(() => {
    getProducts(dispatch);
  }, []);

 const handleClickNext = () =>
 {
     setCurrentPage(currentPage + 1);
     if (currentPage +1 > maxPageNumberLimit){
         setMaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
         setMinPageNumberLimit(minPageNumberLimit + pageNumberLimit);
     }

 }
 const handleClickPrev = () =>
 {
     setCurrentPage(currentPage - 1);
     if ((currentPage - 1)% pageNumberLimit == 0){
         setMaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
         setMinPageNumberLimit(minPageNumberLimit - pageNumberLimit);
     }

 }

  if (isLoading) {
    return (
      <div className="products-wrapper">
        <h1>Loading...</h1>
      </div>
    );
  }

  return (
     <>
         <Slider />
         <FeatureCategory />
        <div className="products-wrapper">
          <div className="products">
            {isLoaded &&
            currentItems.map((data) => {
                return <ProductCard key={data.id} data={data} />;
              })}
          </div>
        </div>

         <div className="d-flex justify-content-center">
             <ul className="pageNumbers">
                 <li>
                     <button onClick={handleClickPrev} disabled={currentPage == pages[0] ? true : false}>
                         Pre
                     </button>
                 </li>
                 {renderPageNumbers}
                 <li>
                     <button onClick={handleClickNext} disabled={currentPage == pages[pages.length-1] ? true : false}>
                         Next
                     </button>
                 </li>

             </ul>
         </div>
     </>
  );
};

export default Home;
