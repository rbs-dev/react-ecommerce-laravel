import React, {useContext, useEffect, useState} from "react";
import { useHistory } from "react-router-dom";
import classNames from "classnames";
import {
  CheckoutStateContext,
  CheckoutDispatchContext,
  CHECKOUT_STEPS,
  setCheckoutStep,
  saveShippingAddress
} from "../contexts/checkout";
import { CartStateContext } from "../contexts/cart";
import { AuthStateContext, AuthDispatchContext, signOut } from "../contexts/auth";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import _get from "lodash.get";
import Input from "../components/core/form-controls/Input";
import { phoneRegExp } from "../constants/common";
import Login from "../components/Login";
import Register from "../components/Register";

const AddressSchema = Yup.object().shape({
  fullName: Yup.string().required("Full Name is required"),
  phoneNumber: Yup.string()
    .required("Phone Number is required")
    .matches(phoneRegExp, "Phone Number is not a valid 10 digit number")
    .min(10, "Phone Number is too short")
    .max(10, "Phone Number is too long"),
  addressLine: Yup.string().required("Door No. & Street is required!"),
  city: Yup.string().required("City is required!"),
  state: Yup.string().required("State is required!"),
  code: Yup.string().required("ZIP/Postal code is required!"),
  country: Yup.string().required("Country is required!")
});

const LoginStep = () => {
  const history = useHistory();
  const { user, isLoggedIn } = useContext(AuthStateContext);
  const authDispatch = useContext(AuthDispatchContext);
  const checkoutDispatch = useContext(CheckoutDispatchContext);
  const handleContinueShopping = () => {
    history.push("/");
  };
  const handleLoginAsDiffUser = () => {
    signOut(authDispatch);
    history.push("/auth");
  };
  /*const handleGotoLogin = () => {
    history.push("/auth");
  };*/
  const handleProceed = () => {
    setCheckoutStep(checkoutDispatch, CHECKOUT_STEPS.SHIPPING);
  };

  /* Login and Register Popup */

    const [isOpen, setIsOpen] = useState(false);
    const [register, setRegister] = useState(false);

    const loginTogglePopup = () => {
        setIsOpen(!isOpen);
        setRegister(false);
    }
    const registerTogglePopup = () => {
        setIsOpen(false);
        setRegister(!register);
    }

    /* User Register Section */

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");

    useEffect(() =>{
        if(localStorage.getItem('user-info')){
            history.push("/order");
        }
    },[])

    async function userRegister(e) {
        e.preventDefault();

        if(name && email && phone && password){
            let item = {name, email, phone, password};
            //console.log(item);
            let result = await fetch("http://www.ecommerce.local/api/register",{
                method:"POST",
                body: JSON.stringify(item),
                headers: {
                    "Content-Type" : "application/json",
                    "Accept" : "application/json"
                }
            });

            result = await result.json();
            console.log("register", result);
            localStorage.setItem("user-info", JSON.stringify(result));
            history.push("/order");

            setName("");
            setEmail("");
            setPhone("");
            setPassword("");

        } else {
            alert("Please Enter the value");
        }
    }

    async function login(e)
    {
        e.preventDefault();
        let login = {email,password};
        //console.log(login);
        let result = await fetch('http://www.ecommerce.local/api/login',{
            method: 'POST',
            body: JSON.stringify(login),
            headers: {
                "Content-Type" : "application/json",
                "Accept" : "application/json"
            }
        });
        result = await result.json();
        console.log("register", result);
        localStorage.setItem("user-info", JSON.stringify(result));
        let user = JSON.parse(localStorage.getItem('user-info'));
        if (user.error)
        {
            alert('Email and password is not matched');
        } else{
            history.push("/order");
        }
        //history.push("/order");
    }


  return (
    <div className="detail-container">
      <h2>Sign In now!</h2>
      <div className="auth-message">
        {isLoggedIn ? (
          <>
            <p>
              Logged in as <span>{user.username}</span>
            </p>
            <button onClick={() => handleLoginAsDiffUser()}>
              Login as Different User
            </button>
          </>
        ) : (
          <>
            <p>Please login to continue.</p>
              <div className="login-btn-center">
                  <button onClick={() => loginTogglePopup()}>Login</button>
              </div>

              {isOpen && <Login
                  content={
                      <>
                          <h3 className="text-center font-weight-bold">Sign In</h3>
                          <form action="">
                              <div className="form-group">
                                  <label htmlFor="email" className="font-weight-bold">Email</label>
                                  <input
                                      type="email"
                                      className="form-control"
                                      name="email" id="email"
                                      placeholder="E-mail"
                                      onChange={(e) =>setEmail(e.target.value)}
                                      value={email}
                                      required
                                  />
                              </div>

                              <div className="form-group">
                                  <label htmlFor="password" className="font-weight-bold">Password</label>
                                  <input
                                      type="password"
                                      className="form-control"
                                      name="password"
                                      id="password"
                                      placeholder="Password"
                                      onChange={(e) =>setPassword(e.target.value)}
                                      value={password}
                                      required
                                  />
                              </div>

                              <div className="login-btn-center">
                                  <button type="submit" className="btn btn-primary" onClick={login}>Sign In</button>
                                  <div className="pt-3">
                                      New User? <a onClick={() => registerTogglePopup()}>Register Here</a>
                                  </div>

                              </div>

                          </form>
                      </>
                  }
                  handleClose={loginTogglePopup}
              />}

              {register && <Register
                  content={
                      <>
                          <h3 className="text-center font-weight-bold">Register</h3>
                          <form action="">
                              <div className="form-group">
                                  <label htmlFor="name" className="font-weight-bold">Full Name</label>
                                  <input
                                      type="text"
                                      className="form-control"
                                      name="name"
                                      id="name"
                                      placeholder="Full Name"
                                      onChange={(e) =>setName(e.target.value)}
                                      value={name}
                                      required
                                  />
                              </div>
                              <div className="form-group">
                                  <label htmlFor="email" className="font-weight-bold">Email</label>
                                  <input
                                      type="email"
                                      className="form-control"
                                      name="email"
                                      id="email"
                                      placeholder="E-mail"
                                      onChange={(e) =>setEmail(e.target.value)}
                                      value={email}
                                      required
                                  />
                              </div>

                              <div className="form-group">
                                  <label htmlFor="phone" className="font-weight-bold">Phone</label>
                                  <input
                                      type="text"
                                      className="form-control"
                                      name="phone"
                                      id="phone"
                                      placeholder="Phone"
                                      onChange={(e) =>setPhone(e.target.value)}
                                      value={phone}
                                      required
                                  />
                              </div>

                              <div className="form-group">
                                  <label htmlFor="password" className="font-weight-bold">Password</label>
                                  <input
                                      type="password"
                                      className="form-control"
                                      name="password"
                                      id="password"
                                      placeholder="Password"
                                      onChange={(e) =>setPassword(e.target.value)}
                                      value={password}
                                      required
                                  />
                              </div>

                             {/* <div className="form-group">
                                  <label htmlFor="confirm_password" className="font-weight-bold">Confirm Password</label>
                                  <input type="password" className="form-control" name="password" id="confirm_password" placeholder="Confirm Password"/>
                              </div>*/}

                              <div className="login-btn-center">
                                  <button type="submit" className="btn btn-primary" onClick={userRegister}>Register</button>
                                  <div className="pt-3">
                                      Already Register? <a onClick={() => loginTogglePopup()}>Sign In</a>
                                  </div>

                              </div>

                          </form>
                      </>
                  }
                  handleClose={registerTogglePopup}
              />}
          </>
        )}
      </div>
      <div className="actions">
        <button className="outline" onClick={() => handleContinueShopping()}>
          <i className="rsc-icon-arrow_back" /> Continue Shopping
        </button>
        <button disabled={!isLoggedIn} onClick={() => handleProceed()}>
          Proceed
          <i className="rsc-icon-arrow_forward" />
        </button>
      </div>
    </div>
  );
};

const AddressStep = () => {
  const checkoutDispatch = useContext(CheckoutDispatchContext);

  const handleBackToLogin = () => {
    setCheckoutStep(checkoutDispatch, CHECKOUT_STEPS.AUTH);
  };
  const handleSaveAddress = (addressData) => {
    saveShippingAddress(checkoutDispatch, addressData);
  };
  return (
    <div className="detail-container">
      <h2>Shipping Address</h2>
      <Formik
        initialValues={{
          fullName: "John Doe",
          phoneNumber: "5552229876",
          addressLine: "L1, Palm Residency",
          city: "Kingston",
          state: "New York",
          code: "12401",
          country: "United States"
        }}
        validationSchema={AddressSchema}
        onSubmit={async (values, { resetForm }) => {
          try {
            const addressData = { ...values };
            resetForm();
            handleSaveAddress(addressData);
          } catch (err) {
            console.error(err);
          }
        }}
      >
        {() => (
          <Form>
            <div className="field-group">
              <Field
                name="fullName"
                type="text"
                placeholder="Full Name"
                component={Input}
              />
              <Field
                name="phoneNumber"
                type="text"
                placeholder="Phone Number"
                component={Input}
              />
            </div>
            <Field
              name="addressLine"
              type="text"
              placeholder="Door No. & Street"
              component={Input}
            />
            <div className="field-group">
              <Field
                name="city"
                type="text"
                placeholder="City"
                component={Input}
              />
              <Field
                name="state"
                type="text"
                placeholder="State"
                component={Input}
              />
            </div>
            <div className="field-group">
              <Field
                name="code"
                type="text"
                placeholder="ZIP/Postal Code"
                component={Input}
              />
              <Field
                name="country"
                type="text"
                placeholder="Country"
                component={Input}
              />
            </div>
            <div className="actions">
              <button
                type="button"
                className="outline"
                onClick={() => handleBackToLogin()}
              >
                <i className="rsc-icon-arrow_back" /> Login in as Different User
              </button>
              <button type="submit">
                Save Address
                <i className="rsc-icon-arrow_forward" />
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

const PaymentStep = () => {
  const { shippingAddress } = useContext(CheckoutStateContext);
  const checkoutDispatch = useContext(CheckoutDispatchContext);
  const handleBackToAddress = () => {
    setCheckoutStep(checkoutDispatch, CHECKOUT_STEPS.SHIPPING);
  };
  const handlePayment = () => {};
  return (
    <div className="detail-container">
      <h2>Payment</h2>
      {/* <div>
        <pre>{JSON.stringify(shippingAddress, null, 0)}</pre>
      </div> */}
      <div className="actions">
        <button
          type="button"
          className="outline"
          onClick={() => handleBackToAddress()}
        >
          <i className="rsc-icon-arrow_back" /> Back to Shipping Details
        </button>
        <button disabled={!shippingAddress} onClick={() => handlePayment()}>
          Save Address
          <i className="rsc-icon-arrow_forward" />
        </button>
      </div>
    </div>
  );
};

const Checkout = () => {
  const { items = [] } = useContext(CartStateContext);
  const { isLoggedIn } = useContext(AuthStateContext);
  const { step, shippingAddress } = useContext(CheckoutStateContext);
  const checkoutDispatch = useContext(CheckoutDispatchContext);
  const totalItems = items.length;
  const cartTotal = items
    .map((item) => item.price * item.quantity)
    .reduce((prev, current) => prev + current, 0);

  const handleClickTimeline = (nextStep) => {
    setCheckoutStep(checkoutDispatch, nextStep);
  };

  return (
    <div className="checkout-page">
      <div className="container">
        <div className="order-details">
          {/*<ul className="timeline">
            <li
              className={classNames({
                done: isLoggedIn,
                active: step === CHECKOUT_STEPS.AUTH
              })}
              onClick={() => handleClickTimeline(CHECKOUT_STEPS.AUTH)}
            >
              <h2>Sign In</h2>
              <i className="rsc-icon-check_circle" />
            </li>
            <li
              className={classNames({
                done: shippingAddress !== null,
                active: step === CHECKOUT_STEPS.SHIPPING
              })}
              onClick={() => handleClickTimeline(CHECKOUT_STEPS.SHIPPING)}
            >
              <h2>Shipping</h2>
              <i className="rsc-icon-check_circle" />
            </li>
            <li
              className={classNames({
                done: false,
                active: step === CHECKOUT_STEPS.PAYMENT
              })}
              onClick={() => handleClickTimeline(CHECKOUT_STEPS.PAYMENT)}
            >
              <h2>Payment</h2>
              <i className="rsc-icon-check_circle" />
            </li>
          </ul>*/}
          {step === CHECKOUT_STEPS.AUTH && <LoginStep />}
          {step === CHECKOUT_STEPS.SHIPPING && <AddressStep />}
          {step === CHECKOUT_STEPS.PAYMENT && <PaymentStep />}
        </div>
        <div className="order-summary">
          <h2>
            Summary
            <span>{` (${totalItems}) Items`}</span>
          </h2>
          <ul className="cart-items">
            {items.map((product) => {
              return (
                <li className="cart-item" key={product.name}>
                  <img className="product-image" src={product.image} />
                  <div className="product-info">
                    <p className="product-name">{product.name}</p>
                    <p className="product-price">{product.price}</p>
                  </div>
                    {/*<div className="stepper-input">
                        <a className="decrement">-</a>
                        <input type="input" className="quantity" placeholder="1"/>
                        <a className="increment">+</a>
                    </div>*/}
                  <div className="product-total">
                    <p className="quantity">
                      {`${product.quantity} ${
                        product.quantity > 1 ? "Nos." : "No."
                      }`}
                    </p>
                    <p className="amount">{product.quantity * product.price}</p>
                  </div>
                </li>
              );
            })}
          </ul>

          <ul className="total-breakup">
            <li>
              <p>Subtotal</p>
              <p>{cartTotal}</p>
            </li>
            {/*<li>*/}
            {/*  <p>Tax</p>*/}
            {/*  <p>10</p>*/}
            {/*</li>*/}
            {/*<li>*/}
            {/*  <p>Shipping</p>*/}
            {/*  <p>40</p>*/}
            {/*</li>*/}
            <li>
              <h2>Total</h2>
              <h2>{cartTotal}</h2>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
