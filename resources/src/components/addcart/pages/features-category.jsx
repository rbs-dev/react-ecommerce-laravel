import React,{useState,useEffect} from 'react';

const FeaturesCategory = () => {

    return(
        <>
            <div className="products-wrapper">
                <div className="category-wrapper">
                    <p className="category-brand">Brand</p>
                    <div className="row">
                        <div className="col-md-8">
                            <div className="row">
                                <div className="col-md-3">
                                    <a href="#">
                                        <div className="brand_item">
                                            <img src="./images/cat 08.jpeg" alt="category"/>
                                            <p className="text-center">BreakFast</p>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-3">
                                    <a href="#">
                                        <div className="brand_item">
                                            <img src="./images/cat 08.jpeg" alt="category"/>
                                            <p className="text-center">BreakFast</p>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-3">
                                    <a href="#">
                                        <div className="brand_item">
                                            <img src="./images/cat 08.jpeg" alt="category"/>
                                            <p className="text-center">BreakFast</p>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-3">
                                    <a href="#">
                                        <div className="brand_item">
                                            <img src="./images/cat 08.jpeg" alt="category"/>
                                            <p className="text-center">BreakFast</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 align-items-center d-flex">View all</div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default FeaturesCategory;
