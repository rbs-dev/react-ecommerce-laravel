import React from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Nav from "../components/Nav";

const CommonLayout = ({ children }) => {
  return (
    <div className="common-layout">
      <Header />
      <Nav />
      <main>{children}</main>
      <Footer />
    </div>
  );
};

export default CommonLayout;
