import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import AuthProvider from "./contexts/auth";
import CommonProvider from "./contexts/common";
import ProductsProvider from "./contexts/products";
import CartProvider from "./contexts/cart";
import CheckoutProvider from "./contexts/checkout";
import RouteWrapper from "./layouts/RouteWrapper";
import AuthLayout from "./layouts/AuthLayout";
import CommonLayout from "./layouts/CommonLayout";
import AuthPage from "./pages/auth";
import HomePage from "./pages/home";
import CheckoutPage from "./pages/checkout";
import FeaturesCategory from "./pages/features-category";
import Search from "./pages/search";
import Order from "./pages/order";
//import "./assets/scss/style.scss";

const App = () => {
    return (
        <AuthProvider>
            <CommonProvider>
                <ProductsProvider>
                    <CartProvider>
                        <CheckoutProvider>
                            <Router>
                                <Switch>
                                    <RouteWrapper
                                        path="/"
                                        exact
                                        component={HomePage}
                                        layout={CommonLayout}
                                    />
                                    <RouteWrapper
                                        path="/checkout"
                                        component={CheckoutPage}
                                        layout={CommonLayout}
                                    />
                                    <RouteWrapper
                                        path="/features-category"
                                        component={FeaturesCategory}
                                        layout={CommonLayout}
                                    />
                                    <RouteWrapper
                                        path="/search"
                                        component={Search}
                                        layout={CommonLayout}
                                    />
                                    <RouteWrapper
                                        path="/order"
                                        component={Order}
                                        layout={CommonLayout}
                                    />
                                    <RouteWrapper
                                        path="/auth"
                                        component={AuthPage}
                                        layout={AuthLayout}
                                    />
                                </Switch>
                            </Router>
                        </CheckoutProvider>
                    </CartProvider>
                </ProductsProvider>
            </CommonProvider>
        </AuthProvider>
    );
};

export default App;
