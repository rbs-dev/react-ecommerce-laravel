import React, { useState, useContext } from "react";
import { CartDispatchContext, addToCart } from "../contexts/cart";
import DetailsPopup from "../components/DetailsPopup";

const Search = ({ data }) => {
    const [isAdded, setIsAdded] = useState(false);
    const dispatch = useContext(CartDispatchContext);
    const { image, name, price, id, stock } = data;
    // console.log(data);
    const handleAddToCart = () => {
        const product = { ...data, quantity: quantity };
        addToCart(dispatch, product);
        setIsAdded(true);
        setTimeout(() => {
            setIsAdded(false);
        }, 3500);
    };

    const [quantity, setQuantity] = useState(1);

    const incrementCounter = (id) => setQuantity(quantity + 1);
    let decrementCounter = (id) => setQuantity(quantity - 1);

    if(quantity<=1) {
        decrementCounter = () => setQuantity(1);
    }

    const [isOpen, setIsOpen] = useState(false);

    const togglePopup = (id) => {
        setIsOpen(!isOpen);
    }

    return (
        <div className="product">
            <div className="product-image">
                <a onClick={ () => togglePopup(id)}>
                    <img src={image} alt={name} />
                </a>
                {isOpen && <DetailsPopup
                    content={<>
                        <div className="show_popup_details">
                            <img className="show_popup_img" src={image} alt={name} />
                        </div>
                        <div className="">
                            <h4 className="product-name float-left">{name}</h4>
                            <p className="product-price float-right">{price}</p>
                        </div>


                    </>}
                    handleClose={togglePopup}
                />}
            </div>
            <h4 className="product-name">{name}</h4>
            <p className="product-price">{quantity * price}</p>
            <div className="stepper-input">
                <a className="decrement" onClick={ () => decrementCounter(id)}>-</a>
                <input type="input" className="quantity" value={quantity}/>
                <a className="increment" onClick={() =>incrementCounter(id)}>+</a>
            </div>
            <div className="product-action">
                <button
                    className={!isAdded ? "" : "added"}
                    type="button"
                    onClick={handleAddToCart}
                >
                    {!isAdded ? "ADD TO CART" : "✔ ADDED"}
                </button>
            </div>
        </div>
    );
};

export default Search;
