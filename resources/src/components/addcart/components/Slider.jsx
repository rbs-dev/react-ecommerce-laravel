import React from 'react';

const Slider = () => {
    return (
        <>
            <section className="slider_section">
                <div className="category_nav">
                    <div className="nav_category">
                        <div className="category_nav_item">
                            <a href="#">Brush Set <i className="right_arrow right"></i></a>
                        </div>
                        <div className="category_nav_item">
                            <a href="#">Groceries & Meals <i className="right_arrow right"></i></a>
                        </div>

                        <div className="category_nav_item">
                            <a href="#">Baby Medi Care <i className="right_arrow right"></i></a>
                        </div>
                        <div className="category_nav_item">
                            <a href="#">Daily Commodities <i className="right_arrow right"></i></a>
                        </div>
                        <div className="category_nav_item">
                            <a href="#">Gastro Lever <i className="right_arrow right"></i></a>
                        </div>
                    </div>
                </div>
                <div className="slider">
                    <div id="carouselExampleIndicators" className="carousel slide slider_top" data-ride="carousel">
                        <ol className="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img className="d-block w-100" src="./images/03.png" alt="First slide"/>
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src="./images/slider_one.jpg" alt="First slide"/>
                            </div>
                            <div className="carousel-item">
                                <img className="d-block w-100" src="./images/slider_two.jpg" alt="Third slide"/>
                            </div>
                        </div>
                        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>

            </section>
        </>
    )
}

export default Slider;
