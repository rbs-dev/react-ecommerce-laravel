import React, {useEffect, useState} from 'react';
import { useHistory } from "react-router-dom";


const FeatureCategory = () => {
    const history = useHistory();

    const handleFeaturesCategory = () => {
        history.push("/features-category");
    };

    const [data, setData] = useState([]);

    useEffect( async() => {
        let result = await fetch('http://www.ecommerce.local/api/feature-category');
        result = await result.json();
        setData(result);
    },[]);

    //console.warn(data);


    return(
        <>
            <section className="feature_section">
                <div className="feature_category">
                    <h4 className="text-uppercase font-weight-bold">Feature Category</h4>
                    <hr/>
                    <div className="category">
                        <div className="row">
                            {
                                data.map((item) => {
                                    return(
                                        <div className="col-12 col-md-3">
                                            <a onClick={handleFeaturesCategory}>
                                                <div className="category_item">
                                                    <img src="./images/cat 01.jpeg" alt="category"/>
                                                    <p className="text-center" key={item.id}>{item.name}</p>
                                                </div>
                                            </a>
                                        </div>

                                    )}
                                )
                            }
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default FeatureCategory;
