import React from 'react';
import {useHistory} from 'react-router-dom';

const Nav = () => {
    let user = JSON.parse(localStorage.getItem('user-info'));
    const history = useHistory();
    function logout() {
        localStorage.clear();
        history.push("/");
    }

    return(
        <>
            <section className="navbar_sections">
                <nav className="navbar navbar-expand-lg navbar-light nav_bg nav-top">
                    <div className="navbar_nav">
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="#">Shop By Category</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Cooking</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Eid Bazar</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Contact</a>
                            </li>

                        </ul>
                        {localStorage.getItem('user-info') ?
                            <div className="form-inline my-2 my-lg-0">
                                <div className='navbar-nav'>
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown"
                                           role="button"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {user && user.name}
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a className="dropdown-item" onClick={logout}>Logout</a>
                                        </div>
                                    </li>
                                </div>
                            </div>
                            : null
                        }
                    </div>
                    </div>
                </nav>

                {/*<nav className="navbar navbar-expand-lg navbar-light nav_bg nav-top">
                    <div className="navbar_nav">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Dropdown
                                    </a>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a className="dropdown-item" href="#">Action</a>
                                        <a className="dropdown-item" href="#">Another action</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                                <li className="nav-item">
                                    <a href="#" className="nav-link shopView d-flex align-items-center">
                                        <div className="menu_icon">
                                            <div className="menu_line"></div>
                                            <div className="menu_line"></div>
                                            <div className="menu_line"></div>
                                        </div>

                                        Shop By Category <i
                                        className="arrow down"> </i></a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link p-18"  href="#">Cooking</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Eid Bazar</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Contact</a>
                                </li>
                            </ul>

                            <div className="logout mr-auto">
                                <ul className="navbar-nav">
                                    <li className="nav-item dropdown">
                                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Dropdown
                                        </a>
                                        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a className="dropdown-item" href="#">Logout</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </nav>*/}
            </section>

        </>
    );
}

export default Nav;
