import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/Home';
import Apps from './components/addcart/App.js'
import Login from './components/Login';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import '../css/app.css';


function App() {
    return (
        <>
            {/*<Router>
                <Route exact path='/' component={Home}/>
                <Route  path="/login" component={Login}/>
            </Router>*/}
            <Apps />

        </>
    );
}

export default App;

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
