<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Models\Gender;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError']]);
    }

    public function showRegistrationForm()
    {
        $genders = Gender::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        return view('auth.register')
            ->with('genders',$genders);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            /*'name' => ['required', 'string', 'max:255'],*/
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
   /* protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }*/


    public function register(Request $request)
    {
         $user = new User();
         $user->first_name = $request->first_name;
         $user->last_name = $request->last_name;
         $user->phone = $request->phone;
         $user->email = $request->email;
         $user->gender_id = $request->gender;
         $user->street_address = $request->street_address;
         $user->postal_code = $request->postal_code;
         $user->password = Hash::make($request->password);
         $user->remember_token = sha1(time());
         $user->save();

         $user->name = $user->getName();
         $user->update();

         if($user != null)
         {
            MailController::sendSignUpEmail($user->first_name, $user->last_name,$user->email,$user->remember_token);
             return redirect()->route('email_verify_page');
            /*return redirect()->back()->with(session()->flash('alert-success','Your Account has be created successfully. Please Check email for verify account.'));*/
         }

        return redirect()->back()->with(session()->flash('alert-danger','Something is wrong.'));
    }

    public function verifyUser(Request $request)
    {
        $remember_token = \Illuminate\Support\Facades\Request::get('code');
        $user = User::where(['remember_token' => $remember_token])->first();
        if($user != null)
        {
            $user->is_active = 1;
            $user->save();
            return redirect()->route('login');
        }
        return redirect()->route('register');
    }

    public function emailVerifyPage()
    {
        return view('auth.verify');
    }
}
