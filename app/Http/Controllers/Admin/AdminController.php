<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin.admin_dashboard');
    }
    public function login(Request $request)
    {
        if($request->isMethod('post')){
            $data = $request->all();

            $rules = [
                'email' => ['required', 'email', 'max:255'],
                'password' => ['required'],
            ];

            $customMessages = [
                'email.required' => 'Email is required.',
                'email.email' => 'Valid Email is required.',
                'password.required' => 'Password is required.',
            ];

            $this->validate($request, $rules, $customMessages);

            if(Auth::guard('admin')->attempt(['email'=>$data['email'],'password' => $data['password']])){
                return redirect('admin/dashboard');
            } else{
                Session::flash('error_msg','Invalid Email or Password');
                return redirect()->back();
            }
        }
        return view('admin.admin_login');
    }

    public function logout()
    {
         Auth::guard('admin')->logout();
         return redirect('/admin/login');
    }

    public function settings()
    {
        //echo "<pre>"; print_r(Auth::guard('admin')->user()); die;
        $adminDetails = Admin::where('email',Auth::guard('admin')->user()->email)->first();
        return view('admin.admin_settings')
            ->with('adminDetails',$adminDetails);
    }
    public function checkCurrentPwd(Request $request)
    {
        $data = $request->all();
       // echo "<pre>"; print_r($data); die();
        if(Hash::check($data['current_pwd'],Auth::guard('admin')->user()->password))
        {
            echo "true";
        } else{
            echo "false";
        }
    }

    public function updateCurrentPwd(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data = $request->all();
           // echo "<pre>"; print_r($data); die;
            if(Hash::check($data['current_pwd'],Auth::guard('admin')->user()->password))
            {
                if ($data['new_pwd'] == $data['confirm_pwd'])
                {
                    Admin::where('id',Auth::guard('admin')->user()->id)->update(['password'=>bcrypt($data['new_pwd'])]);
                    Session::flash('success_msg','Password has been Updated successfully');
                } else{
                    Session::flash('error_msg', 'New Password and Confirm Password Not Match');
                }
            } else{
                Session::flash('error_msg', 'Current Password is incorrect');
            }
            return redirect()->back();
        }

    }

    public function updateAdminDetails(Request $request)
    {
        if($request->isMethod('post'))
        {
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;

            $rules = [
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'phone' => 'required|numeric'
            ];

            $customMessages = [
                'name.required' => 'Name is required',
                'name.alpha' => 'Valid Name is required',
                'phone.required' => 'Phone is required',
                'phone.numeric' => 'Valid Phone is required'
            ];

            $this->validate($request, $rules, $customMessages);

            Admin::where('email', Auth::guard('admin')->user()->email)->update(['name'=>$data['name'],'phone'=>$data['phone']]);
            Session::flash('success_msg','Admin Details Updated successfully');
            return redirect()->back();

        }
        return view('admin.admin_update_details');
    }

}
