<?php

namespace App\Http\Controllers;

use App\Mail\SignupEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public static function sendSignUpEmail($first_name,$last_name,$email,$remember_token)
    {
        $data = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'remember_token' => $remember_token
        ];
        Mail::to($email)->send(new SignupEmail($data));
    }
}
