<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\FeatureCategory;
use App\Models\Gender;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public function userRegister(Request $request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        //$user->phone = $request->input('phone');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return $user;
    }

    public function featureCategory()
    {
        $featureCategories = FeatureCategory::all();
        $returnArray=[];
        if($featureCategories){
            foreach($featureCategories as $featureCategory){
               $returnArray[] = [
                   'id'=>$featureCategory->id,
                   'name'=>$featureCategory->category->name,
                   'image'=>$featureCategory->path
               ];
            }
        }
        return $returnArray;
    }

    public function categoryList()
    {
        $categoryList = DB::table('categories')->get();
        return $categoryList;
    }

    public function search(Request $request)
    {
        //return Category::where('name', 'like', '%'.$name.'%')->get();
        $categorySearch =  Category::where('name', 'like', '%'.$request->name.'%')->get();
        return $categorySearch;
    }
    public function login(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(!$user || !Hash::check($request->password, $user->password))
        {
            return response([
                'error' => 'Email and password is not matched!'
            ]);
        }
        return $user;
    }
}
