<?php

namespace App\Http\Controllers;

use App\Models\Gender;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()
    {
        $user = Auth::user();
        $genders = Gender::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        return view('user.edit_profile')
            ->with('user',$user)
            ->with('genders',$genders);

    }
    public function updateProfile(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->gender_id = $request->gender;
        $user->street_address = $request->street_address;
        $user->postal_code = $request->postal_code;
        $user->update();

        return \Redirect::route('show.profile');
    }

    public function order()
    {
        return view('user.order');
    }
}
