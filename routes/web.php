<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Start Admin Route*/

Route::prefix('/admin')->namespace('Admin')->group(function () {
    Route::match(['get','post'],'/login',[App\Http\Controllers\Admin\AdminController::class, 'login'])->name('admin.login');

    Route::group(['middleware' => ['admin']],function(){
        Route::get('dashboard',[App\Http\Controllers\Admin\AdminController::class, 'dashboard']);
        Route::get('logout',[App\Http\Controllers\Admin\AdminController::class, 'logout'])->name('admin.logout');
        Route::get('settings',[App\Http\Controllers\Admin\AdminController::class, 'settings']);
        Route::post('check-current-pwd',[App\Http\Controllers\Admin\AdminController::class, 'checkCurrentPwd']);
        Route::post('update-current-pwd',[App\Http\Controllers\Admin\AdminController::class, 'updateCurrentPwd']);
        Route::match(['get','post'],'update-admin-details',[App\Http\Controllers\Admin\AdminController::class, 'updateAdminDetails']);

    });

});

/* End Admin Route */



Route::get('/edit-profile',[App\Http\Controllers\UserController::class,'showProfile'])->name('show.profile');
Route::post('/update-profile',[App\Http\Controllers\UserController::class,'updateProfile'])->name('update.profile');

Route::get('/order',[App\Http\Controllers\UserController::class, 'order'])->name('order');

Auth::routes(['verfiy' => true]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/verify',[App\Http\Controllers\Auth\RegisterController::class, 'verifyUser'])->name('verify.user');

Route::get('/verify-link',[App\Http\Controllers\Auth\RegisterController::class, 'emailVerifyPage'])->name('email_verify_page');
